# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
SampleApp::Application.config.secret_key_base = 'ca432b8734d59b959fa7364b1247cf2fceb25e1b59c90c9457140cd9e44e1e0abc0261b3360f8f03c669d275403c4413de4c4075f3fea07396c47a1c6ad2ab70'
